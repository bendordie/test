#include "Settings.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSettings g_Settings;

IMPLEMENT_SERIAL(CSettings, CObject, 1)

CSettings::CSettings()
{
	m_dwPollingPeriod = 1000;
	m_bTestLoopback = FALSE;
	m_bShowSIOMessages = FALSE;
	m_bShowMessageErrors = FALSE;
	m_bShowCOMErrors = FALSE;
	m_strSettingsReportPath = _T("ugs.rep");

	m_nBufferSize = 0x90000;

	m_nIncomingPort = 11112;

	m_strCOMSetup = _T("COM1: baud=9600 data=8 parity=N stop=1");
	m_iCOMRttc = 0;
	m_iCOMWttc = 0;
	m_iCOMRit = -1;

	m_arPrefix.RemoveAll();

	m_wComposedType = 0x000003;
	m_wOutputComposedType = 0x0000;
	m_wCRC16Init = 0xFFFF;

	m_wCPAddr = 0x0000;
	m_wPUAddr = 0x0000;

	m_bUnpackAll = FALSE;
	m_bMarkAll = FALSE;

	m_nStatusPeriod = 0;
	m_iSendStatTO = 1000000;
	m_StatusHdr = MESSAGETYPE(0x0000, 0x20);
	m_StatusMsg = MESSAGETYPE(m_wComposedType);
	m_MarkNestedMask = MESSAGETYPE();
	m_MarkComposedMask = MESSAGETYPE();
	m_arStatusData.RemoveAll();
	MakeStatusMsg();
	UpdateStatusMsg(0);
	m_TUType = 0x000002;
	m_TUSrcMask = 0x0000;
	m_TUSrcComMsgIndex = FALSE;
	m_TUPrimToSecSrc = 1;
	m_TUSecToPrimSrc = 1;


	m_bKeepLog = FALSE;
	m_wLogComposedType = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wLogComposedTypeToPack = 0x0000;
	m_bLogUnpackAll = FALSE;

	m_wSourceID = 0x000020;
	m_wStatusRequestMessageType = 0x0001;

	MESSAGETYPE typeStatus(0x0001, 0x1000);
	m_mapMsgTypes.SetAt(0x0001, typeStatus);
}

CSettings::~CSettings()
{
}

void replDef(unsigned short& expl, const unsigned short& def)
{
  expl = expl ? expl : def;
}


BOOL CSettings::Load(LPCTSTR lpszProfileName)
{
	try
	{
		CStdioFile file(lpszProfileName, CFile::modeRead | CFile::shareDenyWrite | CFile::typeText);

		CMapStringToString mapSettings;
		CString strGroup(_T("[General]"));

		CString strLine;
		while(file.ReadString(strLine))
		{
			strLine.Trim();
			int iComment = strLine.Find(_T(';'), 0);
			if(iComment >= 0)
				strLine.Delete(iComment, strLine.GetLength() - iComment);

			if(strLine.IsEmpty())
				continue;

			if(strLine.Find(_T('[')) == 0 && strLine.ReverseFind(_T(']')) == strLine.GetLength() - 1)
			{
				strGroup = strLine;
				continue;
			}

			int iPos = 0;
			CString strKey(strGroup + _T('/') + strLine.Tokenize(_T("="), iPos).Trim());
			CString strValue(strLine);
			strValue.Delete(0, iPos);

			mapSettings.SetAt(strKey, strValue.Trim().Trim(_T("\"")));
		}

		int iValue;
		if(mapSettings.Lookup(_T("[General]/PollingPeriod"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue) && iValue != 0)
			m_dwPollingPeriod = (DWORD)iValue;

		if(mapSettings.Lookup(_T("[General]/TestLoopback"), strLine))
			m_bTestLoopback = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/ShowSIOMessages"), strLine))
			m_bShowSIOMessages = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/ShowMessageErrors"), strLine))
			m_bShowMessageErrors = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/ShowCOMErrors"), strLine))
			m_bShowCOMErrors = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[General]/SettingsReportPath"), strLine))
			m_strSettingsReportPath = strLine;

		if(mapSettings.Lookup(_T("[General]/BufferSize"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue) && iValue != 0)
			m_nBufferSize = (UINT)iValue;

		if(mapSettings.Lookup(_T("[UDP]/IncomingPort"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue) && iValue != 0)
			m_nIncomingPort = (UINT)iValue;

		if(mapSettings.Lookup(_T("[UDP]/OutgoingIP"), strLine))
		{
			DWORD dwTemp = (DWORD)inet_addr(strLine);
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(strLine);
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}

		}


		if(mapSettings.Lookup(_T("[COM]/SetupParams"), strLine))
			m_strCOMSetup = strLine;

		if(mapSettings.Lookup(_T("[COM]/rttc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iCOMRttc = iValue;

		if(mapSettings.Lookup(_T("[COM]/wttc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iCOMWttc = iValue;

		if(mapSettings.Lookup(_T("[COM]/rit"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iCOMRit = iValue;

		CByteArray arTemp;
		if(mapSettings.Lookup(_T("[Message]/CPAddr"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wCPAddr = (WORD)iValue;
		if(mapSettings.Lookup(_T("[Message]/PUAddr"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wPUAddr = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/Prefix"), strLine) && ByteArrayFromString(strLine, arTemp, _T("")))
			m_arPrefix.Copy(arTemp);

		if(mapSettings.Lookup(_T("[Message]/OutPrefix"), strLine) && ByteArrayFromString(strLine, arTemp, _T("")))
			m_arOutPrefix.Copy(arTemp);

		if(mapSettings.Lookup(_T("[Message]/CRC16Init"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wCRC16Init = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/ComposedType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wComposedType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/OutputComposedType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wOutputComposedType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/TypesToUnPack"), strLine))
		{
			m_mapMsgTypesToUnpack.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapMsgTypesToUnpack.SetAt(0x0000, NULL);
				m_bUnpackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToUnpack.SetAt((WORD)iValue, NULL);
				}
				m_bUnpackAll = FALSE;
			}
		}

		if(mapSettings.Lookup(_T("[Message]/MarkComposedMessageMask"), strLine))
		{
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkComposedMask.m_wSrcMask = (WORD)iValue;
		}
		if(mapSettings.Lookup(_T("[Message]/MarkMessageMask"), strLine))
		{
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_MarkNestedMask.m_wSrcMask = (WORD)iValue;
		}

		if(mapSettings.Lookup(_T("[Message]/TypesToMark"), strLine))
		{
			m_mapMsgTypesToMark.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapMsgTypesToMark.SetAt(0x0000, NULL);
				m_bMarkAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapMsgTypesToMark.SetAt((WORD)iValue, NULL);
				}
				m_bMarkAll = FALSE;
			}
		}

		m_mapMsgTypes.RemoveAll();

		for(int i = 1; i < 10; i++)
		{
			CString strTemp;
			strTemp.Format(_T("[Message]/Type%u"), i);

			if(!mapSettings.Lookup(strTemp, strLine))
				continue;

			MESSAGETYPE type;
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wType = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wMaxLength = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wSource = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wDestMask = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				type.m_wSrcMask = (WORD)iValue;

			if(type.m_wType == 0x0505) {
				replDef(type.m_wSource, m_wPUAddr);
				replDef(type.m_wDestination, m_wCPAddr);
			}
			else if(type.m_wType == 0x0521 || type.m_wType == 0x0532) {
				replDef(type.m_wSource, m_wCPAddr);
			}
			else {
				replDef(type.m_wSource, m_wCPAddr);
				replDef(type.m_wDestination, m_wPUAddr);
			}
			
			m_mapMsgTypes.SetAt(type.m_wType, type);            
		}

		MESSAGETYPE typeStatus(0x0001, 0x1000);
		m_mapMsgTypes.SetAt(0x0001, typeStatus);

		if(mapSettings.Lookup(_T("[Message]/StatusPeriod"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_nStatusPeriod = (UINT)iValue;

		if(mapSettings.Lookup(_T("[Message]/SendStatusTO"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_iSendStatTO = (int)iValue;

		if(mapSettings.Lookup(_T("[Message]/StatusMsg"), strLine))
		{
			int iPos = 0;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wType = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusHdr.m_wSource = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wType = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wDestination = (WORD)iValue;
			if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
				m_StatusMsg.m_wSource = (WORD)iValue;
			ByteArrayFromString(strLine.Tokenize(_T(" "), iPos), arTemp, _T(""));
			m_arStatusData.Copy(arTemp);

			replDef(m_StatusMsg.m_wSource, m_wCPAddr);
			replDef(m_StatusMsg.m_wDestination, m_wPUAddr);

			MakeStatusMsg();
			UpdateStatusMsg(0);
		}

		if(mapSettings.Lookup(_T("[Message]/TUType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_TUType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Message]/TUSrcMask"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_TUSrcMask = (WORD)iValue;
		if(mapSettings.Lookup(_T("[Message]/TUSrcComMsgIndex"), strLine))
			m_TUSrcComMsgIndex = strLine.MakeLower() == _T("1") ? TRUE : FALSE;
		if(mapSettings.Lookup(_T("[Message]/TUPrimToSecSrc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_TUPrimToSecSrc = (UINT)iValue;
		if(mapSettings.Lookup(_T("[Message]/TUSecToPrimSrc"), strLine) && ::StrToIntEx(strLine, STIF_DEFAULT, &iValue))
			m_TUSecToPrimSrc = (UINT)iValue;

		if(mapSettings.Lookup(_T("[Log]/KeepLog"), strLine))
			m_bKeepLog = strLine.MakeLower() == _T("1") ? TRUE : FALSE;

		if(mapSettings.Lookup(_T("[Log]/LogIP"), strLine))
		{
			DWORD dwTemp = (DWORD)inet_addr(strLine);
			if(INADDR_NONE == dwTemp)
			{
				LPHOSTENT lphost;
				lphost = gethostbyname(strLine);
				if (lphost != NULL)
					dwTemp = ((LPIN_ADDR)lphost->h_addr)->s_addr;
				else
				{
					::WSASetLastError(WSAEINVAL);
					dwTemp = (DWORD)ntohl((u_long)INADDR_LOOPBACK);
				}
			}
		}


		if(mapSettings.Lookup(_T("[Log]/LogComposedType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedType = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Log]/LogTypesToUnPack"), strLine))
		{
			m_mapLogMsgTypesToUnpack.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapLogMsgTypesToUnpack.SetAt(0x0000, NULL);
				m_bLogUnpackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToUnpack.SetAt((WORD)iValue, NULL);
				}
				m_bLogUnpackAll = FALSE;
			}
		}

		if(mapSettings.Lookup(_T("[Log]/LogComposedTypeToPack"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wLogComposedTypeToPack = (WORD)iValue;


		if(mapSettings.Lookup(_T("[Log]/LogTypesToPack"), strLine))
		{
			m_mapLogMsgTypesToPack.RemoveAll();
			if(strLine == _T("*"))
			{
				m_mapLogMsgTypesToPack.SetAt(0x0000, NULL);
				m_bLogPackAll = TRUE;
			}
			else
			{
				for(int iPos = 0; iPos < strLine.GetLength() - 1; )
				{
					if(::StrToIntEx(strLine.Tokenize(_T(" "), iPos), STIF_SUPPORT_HEX, &iValue))
						m_mapLogMsgTypesToPack.SetAt((WORD)iValue, NULL);
				}
				m_bLogPackAll = FALSE;
			}
		}

		if(mapSettings.Lookup(_T("[Status]/SourceIndex"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wSourceID = (WORD)iValue;

		if(mapSettings.Lookup(_T("[Status]/StatusRequestMessageType"), strLine) && ::StrToIntEx(strLine, STIF_SUPPORT_HEX, &iValue))
			m_wStatusRequestMessageType = (WORD)iValue;

		return TRUE;
	}
	catch(CFileException * pfe)
	{
		pfe->Delete();
	}

	return FALSE;
}

bool Settings::save(const char *profileName) {

    std::ofstream file;

    file.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    try
    {
        file.open(profileName, std::ios_base::trunc | std::ios_base::out);

        std::stringstream streamTemp;
        std::string       stringTemp;

        file << "[General]" << std::endl;
        file << "PollingPeriod = " << m_dwPollingPeriod << std::endl;
        file << "TestLoopback = " << m_bTestLoopback << std::endl;
        file << "ShowSIOMessages = " << m_bShowSIOMessages << std::endl;
        file << "ShowMessageErrors = " << m_bShowMessageErrors << std::endl;
        file << "ShowCOMErrors = " << m_bShowCOMErrors << std::endl;
        file << "SettingsReportPath = \"" << m_strSettingsReportPath << "\"" << std::endl;

        streamTemp << "BufferSize = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_nBufferSize;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        file << "[UDP]" << std::endl;
        file << "IncomingPort = " << m_nIncomingPort << std::endl;
        file << "SetupParams = \"" << m_strCOMSetup << "\"" << std::endl;

        file << "rttc = " << m_iCOMRttc << std::endl;
        file << "wttc = " << m_iCOMWttc << std::endl;
        file << "rit = " << m_iCOMRit << std::endl;
        file << "[Message]" << std::endl;


        streamTemp << "CPAddr = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wCPAddr;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        streamTemp << "PUAddr = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wPUAddr;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        utils::byteArrayToString(m_arPrefix.data(), m_arPrefix.size(), stringTemp, "Prefix = \"");
        file << stringTemp << "\"";

        streamTemp << "CRC16Init = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wCRC16Init;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        streamTemp << "ComposedType = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wComposedType;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        streamTemp << "OutputComposedType = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wOutputComposedType;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        file << "TypesToUnPack = \"";
        if(m_bUnpackAll)
            file << "*";
        else
        {
            for (auto iter = m_mapMsgTypesToUnpack.begin(); iter != m_mapMsgTypesToUnpack.end(); ++iter)
            {
                streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << iter->first << " ";
                file << streamTemp.str() << std::endl;
                streamTemp.clear();
            }
        }
        file << "\"" << std::endl;
        streamTemp << "MarkComposedMessageMask = \"0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
                   << m_MarkComposedMask.m_wDestMask << " " << m_MarkComposedMask.m_wSrcMask << "\"";
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        streamTemp << "MarkMessageMask = \"0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
                   << m_MarkComposedMask.m_wDestMask << " " << m_MarkComposedMask.m_wSrcMask << "\"";
        file << streamTemp.str() << std::endl;
        streamTemp.clear();
        file << "TypesToMark = \"";
        if(m_bMarkAll)
            file << "*";
        else
        {
            for (auto iter = m_mapMsgTypesToMark.begin(); iter != m_mapMsgTypesToMark.end(); ++iter) {
                streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
                           << iter->first << " ";
                file << streamTemp.str() << std::endl;
                streamTemp.clear();
            }
        }
        file << "\"" << std::endl;


        auto iter = m_mapMsgTypes.begin();
        for (int count = 0; iter != m_mapMsgTypes.end() && count < 10; ++iter, ++count)
        {
            streamTemp << "Type" << count << " = \"";
            streamTemp.seekg(0, std::ios_base::end);
            streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << iter->second.m_wType << " ";
            streamTemp.seekg(0, std::ios_base::end);
            streamTemp << "0x" << std::hex << std::uppercase << iter->second.m_wMaxLength << " ";
            streamTemp.seekg(0, std::ios_base::end);
            streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << iter->second.m_wDestination << " ";
            streamTemp.seekg(0, std::ios_base::end);
            streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << iter->second.m_wSource << " ";
            streamTemp.seekg(0, std::ios_base::end);
            streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << iter->second.m_wDestMask << " ";
            streamTemp.seekg(0, std::ios_base::end);
            streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << iter->second.m_wSrcMask << "\"";
            file << streamTemp.str() << std::endl;
            streamTemp.clear();
        }
        file << "StatusPeriod = " << m_nStatusPeriod << std::endl;
        file << "SendStatusTO = " << m_iSendStatTO << std::endl;
        streamTemp << "StatusMsg = \"";
        streamTemp.seekg(0, std::ios_base::end);
        streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_StatusHdr.m_wType << " ";
        streamTemp.seekg(0, std::ios_base::end);
        streamTemp << "0x" << std::hex << std::uppercase << m_StatusHdr.m_wDestination << " ";
        streamTemp.seekg(0, std::ios_base::end);
        streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_StatusHdr.m_wSource << " ";
        streamTemp.seekg(0, std::ios_base::end);
        streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_StatusMsg.m_wType << " ";
        streamTemp.seekg(0, std::ios_base::end);
        streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_StatusMsg.m_wDestination << " ";
        streamTemp.seekg(0, std::ios_base::end);
        streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_StatusMsg.m_wSource << " ";
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        utils::byteArrayToString(m_arStatusData.data(), m_arStatusData.size(), stringTemp);
        file << stringTemp << std::endl;

        streamTemp << "TUType = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_TUType;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        streamTemp << "TUSrcMask = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_TUSrcMask;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        file << "TUSrcComMsgIndex = " << m_TUSrcComMsgIndex << std::endl;
        file << "TUPrimToSecSrc = " << m_TUPrimToSecSrc << std::endl;
        file << "TUSecToPrimSrc = " << m_TUSecToPrimSrc << std::endl;

        utils::byteArrayToString(m_arOutPrefix.data(), m_arOutPrefix.size(), stringTemp, "OutPrefix = \"");
        file << stringTemp << "\"" << std::endl;
        file << "[Log]" << std::endl;
        file << "KeepLog = " << m_bKeepLog << std::endl;

        streamTemp << "LogComposedType = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wLogComposedType;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        file << "LogTypesToUnPack = \"";
        if(m_bLogUnpackAll)
            file << "*";
        else
        {
            for (auto iter = m_mapLogMsgTypesToUnpack.begin(); iter != m_mapLogMsgTypesToUnpack.end(); ++iter)
            {
                streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
                           << iter->first << " ";
                file << streamTemp.str();
                streamTemp.clear();
            }
        }
        file << "\"" << std::endl;

        streamTemp << "LogComposedTypeToPack = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wLogComposedTypeToPack;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        file << "LogTypesToPack = \"";
        if(m_bLogPackAll)
            file << "*";
        else
        {
            for (auto iter = m_mapLogMsgTypesToPack.begin(); iter != m_mapLogMsgTypesToPack.end(); ++iter)
            {
                streamTemp << "0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
                           << iter->first << " ";
                file << streamTemp.str();
                streamTemp.clear();
            }
        }
        file << "\"" << std::endl;
        file << "[Status]" << std::endl;

        streamTemp << "SourceIndex = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wSourceID;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        streamTemp << "StatusRequestMessageType = 0x" << std::setfill('0') << std::setw(4) << std::hex << std::uppercase << m_wStatusRequestMessageType;
        file << streamTemp.str() << std::endl;
        streamTemp.clear();

        return true;
    }
    catch(const std::ofstream::failure &ex)
    {
        // TODO: smthing
//        pfe->Delete();
    }

    return false;

}

void CSettings::makeStatusMsg()
{
	m_StatusMsg.m_wMaxLength = 16 + (WORD)m_arStatusData.GetSize();
	m_StatusHdr.m_wMaxLength = 16 + m_StatusMsg.m_wMaxLength;
	m_arStatusMsg.SetSize(m_StatusHdr.m_wMaxLength);
	BYTE * pData = m_arStatusMsg.GetData();
	::ZeroMemory(pData, m_StatusHdr.m_wMaxLength);
	WORD * pHeader = (WORD *)pData;
	pHeader[0] = m_StatusHdr.m_wMaxLength;
	pHeader[1] = m_StatusHdr.m_wType;
	pHeader[2] = m_StatusHdr.m_wDestination;
	pHeader[3] = m_StatusHdr.m_wSource;
	pHeader[7] = m_StatusMsg.m_wMaxLength;
	pHeader[8] = m_StatusMsg.m_wType;
	pHeader[9] = m_StatusMsg.m_wDestination;
	pHeader[10] = m_StatusMsg.m_wSource;
	memcpy(pData + 28, m_arStatusData.GetData(), m_arStatusData.GetSize());
}

void CSettings::updateStatusMsg(unsigned char ind)
{
	BYTE * pData = m_arStatusMsg.GetData();
	UINT nLength = (UINT)m_arStatusMsg.GetSize();
	*((DWORD *)(pData + 8)) = *((DWORD *)(pData + 22)) = (DWORD)time(NULL);
	pData[12] = ind;
	pData[26] = ind;
	*((WORD *)(pData + nLength - sizeof(DWORD))) = CRC16(pData + 14, nLength - 14 - sizeof(DWORD), m_wCRC16Init);
	*((WORD *)(pData + nLength - sizeof(WORD))) = CRC16(pData, nLength - sizeof(WORD), m_wCRC16Init);
}
