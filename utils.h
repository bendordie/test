#ifndef TESTIKI_UTILS_H
#define TESTIKI_UTILS_H

#include <string>
#include <sstream>
#include <iomanip>

namespace utils {

    inline std::string   byteToString(unsigned char byByte);
    inline void          byteArrayToString(unsigned char* pData, int iLength, std::string & strResult,
                                           const char* lpszPrefix = "");


    inline std::string byteToString(unsigned char byByte)
    {
        std::stringstream strResult;

        strResult << std::setfill('0') << std::setw(2) << std::hex << std::uppercase << static_cast<int>(byByte);
        return strResult.str();
    }

    inline void byteArrayToString(unsigned char* pData, int iLength, std::string & strResult, const char* lpszPrefix)
    {
        strResult = lpszPrefix;
        for(int i = 0; i < iLength; i++)
            strResult += byteToString(pData[i]);
    }
};


#endif //TESTIKI_UTILS_H
