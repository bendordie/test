#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>
#include "utils.h"

class CSettings : public CObject  
{
	DECLARE_SERIAL(CSettings);

public:

    Settings();
    virtual ~Settings();

    bool   load(const char* profileName);
    bool   save(const char* profileName);
    void   makeStatusMessage();
    void   updateStatusMessage();

    unsigned long  m_dwPollingPeriod;
    bool           m_bTestLoopback;
    bool           m_bShowSIOMessages;
    bool           m_bShowMessageErrors;
    bool           m_bShowCOMErrors;
    std::string    m_strSettingsReportPath;

    unsigned int   m_nBufferSize;

    std::string    m_strIncomingAddress;
    unsigned int   m_nIncomingPort;

    std::string    m_strCOMSetup;
    int            m_iCOMRttc;
    int            m_iCOMWttc;
    int            m_iCOMRit;

    std::vector<unsigned char> m_arPrefix;
    std::vector<unsigned char> m_arOutPrefix;

    unsigned short m_wComposedType;
    unsigned short m_wOutputComposedType;
    unsigned short m_wCRC16Init;
    unsigned short m_wCPAddr;
    unsigned short m_wPUAddr;

    struct MessageType
    {
        unsigned int m_wType;
        unsigned int m_wMaxLength;
        unsigned int m_wDestination;
        unsigned int m_wSource;
        unsigned int m_wDestMask;
        unsigned int m_wSrcMask;

        MessageType(unsigned int wType = 0, unsigned int wMaxLength = 0, unsigned int wDestination = 0,
                    unsigned int wSource = 0, unsigned int wDestMask = 0, unsigned int wSrcMask = 0)
        {
            m_wType = wType;
            m_wMaxLength = wMaxLength;
            m_wDestination = wDestination;
            m_wSource = wSource;
            m_wDestMask = wDestMask;
            m_wSrcMask = wSrcMask;
        }
    };

    std::map<unsigned int, MessageType> m_mapMsgTypes; // CMap<WORD, WORD, MESSAGETYPE, MESSAGETYPE&> m_mapMsgTypes;
    std::map<unsigned int, void*> m_mapMsgTypesToUnpack; // CMapWordToPtr m_mapMsgTypesToUnpack;
    bool m_bUnpackAll;

    std::map<unsigned int, void*> m_mapMsgTypesToMark; // CMapWordToPtr m_mapMsgTypesToMark;
    bool m_bMarkAll;

    unsigned int   m_nStatusPeriod;
    int            m_iSendStatTO;
    MessageType    m_StatusHdr;
    MessageType    m_StatusMsg;
    MessageType    m_MarkNestedMask;
    MessageType    m_MarkComposedMask;

    unsigned int   m_TUType;
    unsigned int   m_TUSrcMask;
    bool           m_TUSrcComMsgIndex;
    unsigned int   m_TUPrimToSecSrc;
    unsigned int   m_TUSecToPrimSrc;

    std::vector<unsigned char>   m_arStatusData;
    std::vector<unsigned char>   m_arStatusMsg;

    bool                            m_bKeepLog;
    unsigned int                    m_wLogComposedType;
    std::map<unsigned int, void*>   m_mapLogMsgTypesToUnpack;
    bool                            m_bLogUnpackAll;

    unsigned int                    m_wLogComposedTypeToPack;
    std::map<unsigned int, void*>   m_mapLogMsgTypesToPack;
    bool                            m_bLogPackAll;

    unsigned int   m_wSourceID;
    unsigned int   m_wStatusRequestMessageType;
};

extern CSettings g_Settings;

#endif // _SETTINGS_H_
